$(document).ready(function()
{	
	"use strict";
	//Nice Scroll Activate
	 $("html").niceScroll();
	$('.carousel').carousel
	(
		{interval:5000}
	);
	//Show Color Option When Click On Gear Icon
	$('.fa-gear').click(function()
	{	/* We Can Use Any One we Want
		$('.color-options').show();
		$('.color-options').fadeIn();
		$('.color-options').toggle();*//*Toggle means show & hide*/
		$('.color-options').fadeToggle();
	}); 

	//Change Theme Color By Tool Box On Click
	var colorLi = $('.color-options ul li');
	colorLi
	.eq(0).css("backgroundColor","#e2c0b8").end()
	.eq(1).css("backgroundColor","red").end()	
	.eq(2).css("backgroundColor","blue").end()
	.eq(3).css("backgroundColor","orange").end()
	.eq(4).css("backgroundColor","yellow");

	colorLi.click(function()
	{
		console.log($(this).attr("data-value"));/*Print Data Value In Console*/
		$("link[href*='theme']").attr("href",$(this).attr("data-value"));
		/* Star means search about theme world*/
		/*go in page and search in the link>>href contents theme then change href 
		attribute to data-value */
	});

	//Show & Hide Button Depend on the height
	var scrollButton = $('.scroll-top');
	$(window).scroll(function()
	{
		//to know height that we want appear button after it
		//console.log($(this).scrollTop());

		if ($(this).scrollTop() >= 600) 
		{
			scrollButton.show();
		}
		else
		{
			scrollButton.hide();
		}
	});

	//Go Top When Click on the button
	scrollButton.click(function()
	{
		$("html,body").animate({scrollTop:0},500)
	});
});

//Loading Screen
/*after load all items in the page*/

$(window).on('load', function()
{
	//$(".loading h1").fadeOut(1000,function()//Fadeout for h1 firstly then fadeout the parent
		$(".loading .spinner").fadeOut(1000,function()
		{
			$(this).parent().fadeOut(1000,function()
			{
				$("body").css("overflow","auto");//Show Scroll after loading
				$(this).remove();//remove loading section from code after loading page
			});
		});
});

//another code to fadeout the layer & spinner at the same time

$(window).on('load', function()
{	
	$("body").css("overflow","auto");//Show Scroll after loading
	$(".loading, loading.spinner").fadeOut(2000,function()
			{
				$(this).remove();//remove loading section from code after loading page
			});
});